#![feature(custom_derive, plugin, type_ascription)]
#![plugin(serde_macros)]

extern crate serde;
extern crate serde_yaml;
extern crate git2;
#[macro_use] extern crate clap;
extern crate mktemp;
extern crate tremor;

use std::fs::File;
use serde::de::Deserialize;
use serde_yaml::from_reader;
use git2::Repository;
use clap::App;
use mktemp::Temp;
use std::process::Command;
use tremor::*;


#[derive(Default, Deserialize)]
struct Conf {
  directories : DirectoryConf
}

#[derive(Deserialize)]
struct DirectoryConf {
  artifacts : String,
  gits : String,
  tmp : String
}

impl Default for DirectoryConf {
  fn default() -> DirectoryConf {
    DirectoryConf { artifacts: String::from("artifacts"),
                    gits: String::from("gits"), 
                    tmp: String::from("tmp") }
    }
}

fn build_system(system: System, config: Conf) {
  for x in system.strata.iter() {
    for y in x.chunks.iter() {

      let repo_path = format!("{}/{}", config.directories.gits, y.name.as_str());
      println!("Cloning {}", y.name);

      Repository::clone(y.repo.as_str(), repo_path.as_str());
      let mut temp_dir = Temp::new_dir().unwrap();
      let path_buf_str = temp_dir.to_path_buf();
      let build_dir = format!("{}/{}.build", path_buf_str.to_str().unwrap(), y.name);
      Repository::clone(repo_path.as_str(), build_dir.as_str());

      for z in y.to_owned().bit.configure_commands.unwrap().iter() {
        println!("{}", z);
        let output = Command::new("bwrap")
                             .args(&["--bind", "/", "/"])
                             .args(&["--dev", "/dev"])
                             .args(&["--proc", "/proc"])
                             .args(&["--chdir", build_dir.as_str()])
                             .arg("bash").arg("-c").arg(z)
                             .output()
                             .expect("failed to execute process");
        println!("{}", String::from_utf8_lossy(&output.stdout));
      }
      temp_dir.release();
    }
  }
}

fn main() {
  let yaml = load_yaml!("cli.yaml");
  let matches = App::from_yaml(yaml).get_matches();

  let c : Conf = matches.value_of("config").map_or(Default::default(), |x| from_reader(File::open(x).unwrap()).unwrap());

  if let Some(matches) = matches.subcommand_matches("build") {
    if let Some(system) = matches.value_of("system") {
      build_system(from_reader(File::open(system).unwrap()).unwrap(), c);
    }
  }
}
